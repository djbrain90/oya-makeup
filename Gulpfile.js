'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function (done) {
    gulp.src('./scss/**/*.scss')
        .pipe(sass(/*{outputStyle: 'compressed'}*/).on('error', sass.logError))
        .pipe(autoprefixer({cascade: false}))
        .pipe(gulp.dest('./css'));
    done();
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', gulp.series('sass'));
});

gulp.task('scripts', function () {
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'libs/swiper-bundle.min.js',
        'js/main.js',
        // 'js/swiper.js',
        //'js/popup-video.js',

    ])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});