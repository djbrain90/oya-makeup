(function () {
  const $popup = document.querySelector('.popup-video');
  const $closeBtn = document.querySelector('.popup-video__close');
  const $box = document.querySelector('.popup-video__box');
  const $btns = document.querySelectorAll('[data-js-video]');
  const $prev = document.querySelector('.popup-video__navigation-prev');
  const $next = document.querySelector('.popup-video__navigation-next');

  // 1
  generateMarkup();

  // 2
  const slider = window.initSwiper($box, 'popupVideo');

  // 3
  if (slider) {
    slideChange();
    slider.on('transitionEnd', slideChange);
  }

  // 4 show popup
  $btns.forEach(($el) => {
    $el.addEventListener('click', (e) => {
      e.preventDefault();
      $popup.classList.add('is-open');
      slider.slideTo(e.target.dataId);
    });
  });

  // 5 hide popup
  $closeBtn.addEventListener('click', () => {
    $popup.classList.remove('is-open');
    removeIframe();
  });

  function slideChange() {
    removeIframe();
    slider.slides.forEach(($el) => {
      const dataName = $el.dataset.videoPopupName;
      const id = $el.dataset.videoPopupId;

      if ($el.classList.contains('swiper-slide-prev')) {
        $prev.style.setProperty('--bg', `url(${window[dataName][id]['img']})`);
      }

      if ($el.classList.contains('swiper-slide-next')) {
        $next.style.setProperty('--bg', `url(${window[dataName][id]['img']})`);
      }
    });
  }

  function generateMarkup() {
    const $wrapper = document.querySelector('.popup-video__list');
    const itemTpl = `
    <li class="popup-video__item swiper-slide"
        data-video-popup-id="{{id}}"
        data-video-popup-name="{{name}}">
      <div class="popup-video__item-title">{{title}}</div>

      <div class="popup-video__item-box">
        <a href="{{link}}" class="popup-video__item-link">
          <div class="popup-video__item-poster">
            <img src="{{poster}}" alt="" class="popup-video__item-poster" />
          </div>
        </a>
      </div>
    </li>
          `;

    let items = '';

    $btns.forEach(($el, i) => {
      const [data, id, dataName] = getData($el);
      if (!data) return;

      $el.dataId = i;

      let item = itemTpl.replace('{{id}}', id);

      item = item.replace('{{name}}', dataName);
      item = item.replace('{{title}}', data.title);
      item = item.replace('{{link}}', 'https://youtu.be/' + data.youtubeId);
      item = item.replace('{{poster}}', data.posterUrl);

      items += item;
    });

    $wrapper.insertAdjacentHTML('afterbegin', items);

    $wrapper.querySelectorAll('.popup-video__item-link').forEach(($el) => {
      $el.addEventListener('click', replaceByIframe);
    });
  }

  function replaceByIframe(e) {
    e.preventDefault();

    const $link = e.currentTarget;

    $link.insertAdjacentHTML(
      'afterend',
      `<iframe src="https://www.youtube.com/embed/${$link.href.replace(
        'https://youtu.be/',
        '',
      )}?autoplay=1" allow='autoplay'></iframe>`,
    );
    $link.hidden = true;
  }

  function removeIframe() {
    slider.slides.forEach(($el) => {
      const $iframe = $el.querySelector('iframe');
      const $link = $el.querySelector('.popup-video__item-link');

      if ($iframe) {
        $iframe.remove();
        $link.hidden = false;
      }
    });
  }

  function getData($el) {
    if (!$el.dataset.jsVideo) {
      return;
    }

    let [data, id] = $el.dataset.jsVideo.split(',');

    data = (data && data.trim()) || null;
    id = (id && id.trim()) || null;

    if (!(data && id)) {
      return;
    }

    return (window[data] && [window[data][id], id, data]) || null;
  }
})();
