/* global Swiper */
(function () {
  const $swipers = document.querySelectorAll('[data-js-swiper]');
  $swipers.forEach(initSwiper);
})();

function initSwiper($el, optionsName) {
  const data =
    (window.swiperOptions &&
      window.swiperOptions[optionsName || $el.dataset.jsSwiper]) ||
    {};

  const options = {
    loop: false,
    roundLengths: true,
    setWrapperSize: true,
    slidesPerView: 1,
    spaceBetween: 30,
    ...data,
  };

  // eslint-disable-next-line no-new
  return new Swiper($el, options);
}
