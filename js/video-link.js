(function () {
  const $links = document.querySelectorAll('[data-js-video-id]');

  // item = item.replace('{{link}}', 'https://youtu.be/' + data.youtubeId);

  $links.forEach(($link) => {
    $link.addEventListener('click', addIframe);
  });

  function addIframe(e) {
    e.preventDefault();

    const $link = e.currentTarget;
    const id = $link.dataset.jsVideoId;

    $link.insertAdjacentHTML(
      'afterend',
      `<iframe src="https://www.youtube.com/embed/${id}?autoplay=1" allow='autoplay'></iframe>`,
    );

    $link.hidden = true;
    $link.removeEventListener('click', addIframe);
  }
})();
