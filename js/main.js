window.onload = function () {
  "use strict";

  $(function(){
    let body = $('.body');

    $(document).on('click', '.js-show-join-wl', function (e) {
      e.preventDefault(e);
      body.toggleClass('_fixed');
      $('.join-whitelist').toggleClass('_open');
    });
  });

  const promoSlider = new Swiper('.js-pillars-slider', {


    slidesPerView: 1.2,
    loop: false,
    spaceBetween: 10,
    speed: 700,
    //cssMode: true,

    navigation: {
      nextEl: '.slider-button-next',
      prevEl: '.slider-button-prev',
    },

    breakpoints: {

      577: {
        spaceBetween: 20
      },
      769: {
        slidesPerView: 1.8,
        spaceBetween: 40
      },
      1381: {
        slidesPerView: 2,
        spaceBetween: 60
      }
    },

  });
  const journeySlider = new Swiper('.js-journey-slider', {

    centeredSlides: true,
    slidesPerView: 1.6,
    initialSlide: 1,
    loop: true,
    speed: 700,


    navigation: {
      nextEl: '.slider-button-next',
      prevEl: '.slider-button-prev',
    },

    breakpoints: {

      577: {
        slidesPerView: 2.3,
      },

    },

  });


};


$(function () {

  let width = $(window).width();
  let body = $('.body');

  //sliders


  if (width>1024){
    const locationsSlider = new Swiper('.js-locations-slider', {


      speed: 400,
      initialSlide: 1,
      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
  
  
      navigation: {
        nextEl: '.slider-button-next',
        prevEl: '.slider-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        renderFraction: function (currentClass, totalClass) {
          return '<span class = "' + currentClass + '" ></span>  - <span class ="' + totalClass + '">'
        }
      },
      on: {
        init: function () {
          $('.locations__imgs').fadeOut(200);
          $(`.locations__imgs[id = "locationImg-${1 + this.activeIndex}"]`).fadeIn(200);
        },
      },
    });
  
    locationsSlider.on('slideChange', function () {
      let marks = document.querySelectorAll(".js-locations-mark");
  
      $('.locations__imgs').fadeOut(200);
  
      $(`.locations__imgs[id = "locationImg-${1 + locationsSlider.activeIndex}"]`).fadeIn(200);
  
      marks.forEach((el) => {
        el.classList.remove('_active');
        let numId = 1 + locationsSlider.activeIndex;
        let currentId = "mark-" + numId;
        document.getElementById(currentId).classList.add('_active');
      })
  
    });
  
  }
  
  const cryptoSlider = new Swiper('.js-crypto-access-slider', {

    speed: 700,
    spaceBetween: 5,

    breakpoints: {

      577: {
        spaceBetween: 320,
      },

    },

  });

  cryptoSlider.on('slideChange', function () {

    $('.js-crypto-slide-move').removeClass('_active');
    let numId = cryptoSlider.activeIndex;
    $(`.js-crypto-slide-move[slideTo = ${numId}]`).addClass('_active');
    $('.crypto-access__info').removeClass('_show');
    $('.crypto-access__btn').html('View utilities');
    $('.crypto-access__btn').removeClass('_active');
  });




  $('.js-crypto-slide-move').on('click', function () {
    let indexSlide = $(this).attr('slideTo');
    cryptoSlider.slideTo(indexSlide);
  });

  //sliders end





  $(document).on('click', '.js-toggle-menu', function (e) {
    e.preventDefault(e);
    body.toggleClass('_fixed');
    $('.menu').toggleClass('_open');
    $('.header__content').toggleClass('_hide');
    $('.burger').toggleClass('_active');
  });

  //tabs
  $(document).on('click', '.js-locations-tabs', function (e) {
    e.preventDefault();
    $('.js-locations-tabs').removeClass('_active');
    $(this).addClass('_active');
    let currentLink = $(this).attr('href');
    let currentLinkImg = $(this).attr('hrefImg');

    $('.locations__tabs-box').removeClass('_active');
    $('.locations__imgs').removeClass('_active');
    $(currentLink).addClass('_active');
    $(currentLinkImg).addClass('_active');
  });
  //tabs

  //crypto-access
  $(document).on('click', '.js-info-toggle', function (e) {
    if ($(this).hasClass('_active')) {

      if (!$(this).attr('close')) {
        $(this).html('View utilities');
      } else {
        $('.crypto-access__info').removeClass('_show');
        $('.crypto-access__btn').html('View utilities');
        $('.crypto-access__btn').removeClass('_active');
      }
      $(this).removeClass('_active');

    } else {
      if (!$(this).attr('close')) {
        $(this).html('close');
      } else {
        $('.crypto-access__info').removeClass('_show');
        $('.crypto-access__btn').html('View utilities');
        $('.crypto-access__btn').removeClass('_active');
      }
      $(this).addClass('_active');
    }


    let currentLink = $(this).attr('openId');
    $(currentLink).toggleClass('_show');


  });
  //crypto-access

  // dropdown menu
  $(document).on('click', '.js-menu-dropdown', function (e) {
    $('.menu__list').toggleClass('_hide');
    $('.menu__dropdown').toggleClass('_show');
    $('.menu__right').toggleClass('_hide');
  });

  if($(window).scrollTop() < 100){
    $('#scroll-top-page').fadeOut();
  }

  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
      $('#scroll-top-page').fadeIn();
    } else {
      $('#scroll-top-page').fadeOut();
    }
  });

  $('#scroll-top-page').on('click', function () {
    $("html, body").animate({scrollTop: 0}, 700);
    return false;
  });
});